@extends('layouts.app')
@section('content')
    @include('teacher.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Your Booking
            </h1>

            <div class="portlet light calendar bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <i class="icon-calendar font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Calendar</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="drivi-calendar"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('includes.calendar')