@extends('layouts.app')
@section('content')
    @include('teacher.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Your profile</h1>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <th>Name:</th>
                                        <td> {{$user->name}} </td>
                                    </tr>
                                    <tr>
                                        <th>Email:</th>
                                        <td> {{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>Efternavn:</th>
                                        <td> {{$user->data->last_name}} </td>
                                    </tr>
                                    <tr>
                                        <th>Telefon:</th>
                                        <td> {{$user->data->phone}} </td>
                                    </tr>
                                    <tr>
                                        <th>Adresse:</th>
                                        <td> {{$user->data->address}} </td>
                                    </tr>
                                    <tr>
                                        <th>Adresse 2:</th>
                                        <td> {{$user->data->address2}} </td>
                                    </tr>
                                    <tr>
                                        <th>Postnr:</th>
                                        <td> {{$user->data->post_index}} </td>
                                    </tr>
                                    <tr>
                                        <th>By	:</th>
                                        <td> {{$user->data->city}} </td>
                                    </tr>
                                    <tr>
                                        <th>Team:</th>
                                        @if($user->teamTeacher)
                                            <td> {{$user->teamTeacher->name}} </td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Date the  enrolled </th>
                                        <td> {{$user->created_at->format('d M Y')}} </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection