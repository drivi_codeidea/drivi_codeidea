<div class="form-body">
    {{ csrf_field()}}
    <div class="form-body">
        {!! Form::hidden('email', null, ['class' => 'form-control']) !!}
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('Fornavn') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('Password') !!}
            <span class="required" aria-required="true"> * </span>
            <input type="password" class="form-control" name="old_password" >
        </div>
        <div class="form-group">
            {!! Form::label('New Password') !!}
            <input type="password" class="form-control" name="new_password" >
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            {!! Form::label('Confirm Password') !!}
            <input type="password" class="form-control" name="password" >
            <span class="text-danger">{{ $errors->first('password') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('Efternavn') !!}
            {!! Form::text('data[last_name]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Telefon') !!}
            {!! Form::number('data[phone]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Adresse') !!}
            {!! Form::text('data[address]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Adresse 2') !!}
            {!! Form::text('data[address2]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Postnr') !!}
            {!! Form::number('data[post_index]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('By') !!}
            {!! Form::text('data[city]', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-actions">
        {!! Form::submit($button_label, ['class' => 'btn blue']) !!}
    </div>
</div>