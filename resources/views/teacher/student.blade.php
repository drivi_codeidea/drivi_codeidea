@extends('layouts.app')
@section('content')
    @include('teacher.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Elev
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">

                        <div class="portlet-body">
                            <table class="table table-striped table-hover table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th>CPR NR. </th>
                                    <th> NAVN </th>
                                    <th> EFTERNAVN </th>
                                    <th> TELEFON </th>
                                    <th> EMAIL </th>
                                    {{--<th> LECTION PLAN </th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                <tr>
                                    <td>{{$student->data->code}}</td>
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->data->last_name}}</td>
                                    <td>{{$student->data->phone}}</td>
                                    <td>{{$student->email}}</td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    </div>
@endsection
@include('includes.ordering')