@section('scripts')
    <script>
        $(document).ready(function() {
            $('#drivi-calendar').fullCalendar({
                events: [
                    @foreach($bookings as $booking)
                    {
                        title: '{{$booking->type}}',
                        start: '{{$booking->date}}T{{$booking->time_start}}',
                        backgroundColor: App.getBrandColor('red')
                    },
                    @endforeach
                ]
            });
        });
    </script>
@endsection