
@if(session()->has('success'))
    <script>
        toastr.success('{{ session()->get('success') }}',
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "100",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "hide"
        });
    </script>
@endif
@if (session()->has('error'))
    <script>
        toastr.error('{{ session()->get('error') }}',
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "hide"
        });
    </script>
@endif
@if (isset($errors) && count($errors) > 0)
    <script>
        @foreach ($errors->all() as $error)
        toastr.error('{{ $error }} ',
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "hide"
            });
        @endforeach
    </script>
@endif

