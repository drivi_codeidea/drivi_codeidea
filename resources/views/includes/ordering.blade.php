@section('scripts')
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                'order': [[1, 'desc']],
            });
        });
    </script>
@endsection