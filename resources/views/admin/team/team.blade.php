@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Team
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">

                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a href="{{route('team.create')}}" class="btn green"> Create New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th>NAME </th>
                                    <th> MEMBERS </th>
                                    <th> Delete </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teams as $team)
                                <tr>
                                    <td><a href="{{route('team.edit', $team->id)}}">{{$team->name}}</a> </td>
                                    <td>
                                        <b>Lærer:</b> <a href="{{route('teacher.show', $team->teacher->id)}}"> {{$team->teacher->name}}</a> <br>
                                        <b>Elev:</b> @foreach($team->students as $student) <a href="{{route('student.show', $student->id)}}">{{$student->name}}, </a> @endforeach </td>
                                    <td>
                                        {!! Form::open(['url' => route('team.destroy',$team->id), 'id' => 'delete-form-'.$team->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                                        {{Form::close()}}
                                        <a href="#team-{{$team->id}}" data-toggle="modal" > <i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <div class="modal fade draggable-modal" id="team-{{$team->id}}" tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            </div>
                                            <div class="modal-body"> Are you sure, You Want to delete this? </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                <a  type="button"   onclick=document.getElementById('delete-form-{{ $team->id }}').submit();
                                                        " class="btn green">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    </div>
@endsection
@include('includes.ordering')



