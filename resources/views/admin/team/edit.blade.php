@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')

    <div class="page-content-wrapper">
        <div class="page-content">
            <h1 class="page-title"> Rediger team
            </h1>
            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo col-md-10">
                                <a href="{{route('team.index')}}" class="caption-subject bold uppercase btn btn-info"> <i class="fa fa-long-arrow-left"></i> Back to Team</a>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!! Form::model($team, ['url' => route('team.update', $team->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
                        @include('admin.team._partials._form', ['button_label' => 'Edit', 'teamSelectedStudents' => $team->students->pluck('id')])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection