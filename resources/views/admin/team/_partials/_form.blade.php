<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some  errors. Please check below. </div>
    <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! </div>
    {{ csrf_field()}}
    <div class="form-body">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label>Team name  <span class="required"> * </span> </label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
            <label>Kommentar <span class="required"> * </span></label>
            {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => '10']) }}
            <span class="text-danger">{{ $errors->first('description') }}</span>
        </div>
        <div class="form-group {{ $errors->has('teacher_id') ? 'has-error' : '' }}">
            <label><h3>Lærer</h3> <span class="required"> * </span></label>
            {{ Form::select('teacher_id', $teachers, null, ['class' => 'form-control']) }}
            <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
        </div>
        <div class="portlet-body">
            <div class="form-group {{ $errors->has('student_id') ? 'has-error' : '' }}">
                <label for="multiple" class="control-label"><h3>Elev</h3><span class="required"> * </span></label>
                {{ Form::select('student_id[]', $students, $teamSelectedStudents, ['class' => 'form-control select2-multiple select-student', 'id' => 'multiple', 'multiple']) }}
                <span class="text-danger">{{ $errors->first('student_id') }}</span>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue">{{$button_label}}</button>
        </div>
    </div>
</div>
@section('scripts')
<script>
    $(function () {
        $('.select-student').select2();
    })
</script>
@endsection
