@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Tilføj ny team
            </h1>
            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <i class="fa fa-long-arrow-left"></i>
                                <a href="{{route('team.index')}}" class="caption-subject bold uppercase"> Back to Team</a>
                            </div>
                        </div>
                        <div class="portlet-body ">
                            {{ Form::open([ 'route' => 'team.store',  'class' => 'form-group',  'id' => 'form_sample_2', 'multiple'])}}
                            @include('admin.team._partials._form', ['button_label' => 'Save', 'teamSelectedStudents' => null])
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


