@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')

        @switch($bookingType->name)
            @case('Single practical')
            {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._single_form', ['button_label' => 'Save'])
            {{Form::close()}}
            @break
            @case('Theretical team booking')
            {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._theretical_form', ['button_label' => 'Save'])
            {{Form::close()}}
            @break
            @case('Practical team booking')
            {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._practical_form', ['button_label' => 'Save'])
            {{Form::close()}}
            @break
            @case('Manøvrebane')
            {{ Form::open([ 'route' => 'booking.storeStudent',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._manovrebane_form', ['button_label' => 'Save', 'bookingSelectedStudents' => null])
            {{Form::close()}}
            @break
            @case('Glatbane')
            {{ Form::open([ 'route' => 'booking.storeStudent',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._glatbane_form', ['button_label' => 'Save', 'bookingSelectedStudents' => null])
            {{Form::close()}}
            @break
            @case('Theoretical test')
            {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._theoretical_test_form', ['button_label' => 'Save'])
            {{Form::close()}}
            @break
            @case('Practical test')
            {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.booking._partials._practical_test_form', ['button_label' => 'Save'])
            {{Form::close()}}
            @break
        @endswitch

@endsection