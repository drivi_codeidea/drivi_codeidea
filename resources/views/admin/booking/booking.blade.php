@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Booking</h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                                <a class="btn btn-primary" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                                    <i class="fa fa-plus"></i> Create new booking
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        @foreach($types as $type)
                                                            <a href="{{route('booking.create', $type->id)}}">{{$type->name}} </a>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                        </div>
                                        <div class="btn-group">
                                                <a class="btn green" href="{{route('booking.calendar')}}"><i class="fa fa-calendar-plus-o"></i> Calendar</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <a href="{{route('booking.archive')}}" class="btn green btn-outline " >
                                                <i class="fa fa-archive"></i>
                                                Archive
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th> TYPE </th>
                                    <th> DATO </th>
                                    <th> TID </th>
                                    <th> KOMMENTAR </th>
                                    <th> Delete </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bookings as $booking)
                                <tr>
                                    <td><a href="{{route('booking.edit', $booking->id)}}">{{$booking->type}} </a></td>
                                    <td><a href="{{route('booking.edit', $booking->id)}}">{{$booking->date}} </a></td>
                                    <td><a href="{{route('booking.edit', $booking->id)}}">{{$booking->time_start}} - {{$booking->time_end}} </a></td>
                                    <td class="center"><a href="{{route('booking.edit', $booking->id)}}">  {{$booking->description}}</a></td>
                                    <td>
                                        {!! Form::open(['url' => route('booking.destroy',$booking->id), 'id' => 'delete-form-'.$booking->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                                        {{Form::close()}}
                                        <a href="#booking-{{$booking->id}}" data-toggle="modal" > <i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <div class="modal fade draggable-modal" id="booking-{{$booking->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            </div>
                                            <div class="modal-body"> Are you sure, You Want to delete this? </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                <a  type="button"   onclick="
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $booking->id }}').submit();
                                                    " class="btn green">Delete
                                                </a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@include('includes.ordering')