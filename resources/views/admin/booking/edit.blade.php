@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')

        @switch($booking->type)
            @case('Single practical')
                {!! Form::model($booking, ['url' => route('booking.update', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._single_form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            @break
            @case('Theretical Team booking')
                {!! Form::model($booking, ['url' => route('booking.update', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._theretical_form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            @break
            @case('Practical Team booking')
                {!! Form::model($booking, ['url' => route('booking.update', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._practical_form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            @break
            @case('Manøvrebane')
                {!! Form::model($booking, ['url' => route('booking.updateStudent', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._manovrebane_form', ['button_label' => 'Edit', 'bookingSelectedStudents' => $bookSelectedStudents->students->pluck('id')])
                {!! Form::close() !!}
            @break
            @case('Glatbane')
                {!! Form::model($booking, ['url' => route('booking.updateStudent', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._glatbane_form', ['button_label' => 'Edit', 'bookingSelectedStudents' => $bookSelectedStudents->students->pluck('id')])
                {!! Form::close() !!}
            @break
            @case('Theoretical test')
                {!! Form::model($booking, ['url' => route('booking.update', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._theoretical_test_form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            @break
            @case('Practical test')
                {!! Form::model($booking, ['url' => route('booking.update', $booking->id), 'method' => 'PUT']) !!}
                    @include('admin.booking._partials._practical_test_form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            @break
        @endswitch

@endsection