@extends('layouts.app')
@section('content')

    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title" > Bookings</h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a class="btn btn-primary" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            <i class="fa fa-plus"></i> Create new booking
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                @foreach($types as $type)
                                    <a href="{{route('booking.create', $type->id)}}">{{$type->name}} </a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                    <a href="{{route('booking.index')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <div class="portlet light calendar bordered">
                <div class="portlet-body">
                    <div id="drivi-calendar"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('includes.calendar')