@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <h1 class="page-title"> Tilføj ny
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            {{ Form::open([ 'route' => 'price.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                 @include('admin.price._partials._form', ['button_label' => 'Save'])
            {{Form::close()}}
        </div>
    </div>

@endsection