{{ csrf_field() }}
<div class="form-body">
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {{Form::label('Name')}}
        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-get-pocket"></i>
            </span>
            {!! Form::number('name', null, ['class' => 'form-control']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('name') }}</span>
    </div>
    <div class="form-actions">
        {!! Form::submit($button_label, ['class' => 'btn blue']); !!}
    </div>
</div>