@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <h1 class="page-title"> Rediger Kursus priser
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            {!! Form::model($price, ['url' => route('price.update', $price->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
            @include('admin.price._partials._form', ['button_label' => 'Edit'])
            {{Form::close()}}
        </div>
    </div>

@endsection