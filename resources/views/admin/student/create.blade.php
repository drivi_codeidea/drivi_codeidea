@extends('layouts.app')
@section('content')
@include('admin.layouts.sidebar')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h1 class="page-title"> Tilføj ny
            </h1>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">

                                <a href="{{route('student.index')}}" class="btn btn-default caption-subject bold uppercase"> <i class="fa fa-long-arrow-left"></i> Back to Elev</a>
                            </div>
                        </div>
                        <div class="portlet-body ">
                            {{ Form::open([ 'route' => 'student.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                                @include('admin.student._partials._form', ['button_label' => 'Save'])
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection