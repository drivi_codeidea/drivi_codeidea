@extends('layouts.app')
@section('content')
@include('admin.layouts.sidebar')

    <div class="page-content-wrapper">
        <div class="page-content">
            <h1 class="page-title"> Rediger Elev</h1>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="col-md-9">
                        <a href="{{route('student.index')}}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back To Students</a>
                        <a href="{{route('student.show', $student->id)}}" class="btn btn-info"><i class="fa fa-user"></i> View Student</a>
                        <br><br>
                        <a href="{{route('student.add_booking', $student->id)}}" class="btn btn-info"><i class="fa fa-plus"></i> Add Booking</a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('student.calendar', $student->id)}}" class="btn btn-info"><i class="fa fa-calendar-plus-o"></i> Bookings</a>
                        <a href="" class="btn btn-info"><i class="fa fa-list-ul"></i> Lection Plan</a><br><br>
                        <a href="#student-{{$student->id}}" data-toggle="modal" class="btn btn-warning"><i class="fa fa-book"></i> Archive </a>
                        {!! Form::open(['url' => route('student.destroy',$student->id), 'id' => 'delete-form-'.$student->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                        {{Form::close()}}
                        <div class="modal fade draggable-modal" id="student-{{$student->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body"> Are you sure you want to archive {{$student->name}}? </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                                        <a onclick="document.getElementById('delete-form-{{ $student->id }}').submit();" class="btn green">Yes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body ">
                {!! Form::model($student, ['url' => route('student.update', $student->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
                    @include('admin.student._partials._form', ['button_label' => 'Edit'])
                {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>

@endsection