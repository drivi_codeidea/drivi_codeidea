<div class="form-body">
    {{ csrf_field()}}
    <div class="form-body">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('Fornavn') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group">
            {!! Form::label('Efternavn') !!}
            {!! Form::text('data[last_name]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('Email') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
            <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            {!! Form::label('Password') !!}
            <input type="password" class="form-control" name="password" value="{{old('password')}}">
            <span class="text-danger">{{ $errors->first('password') }}</span>
        </div>
        {!! Form::hidden('role', 'student') !!}
        <div class="form-group">
            {!! Form::label('Kursus typer') !!}
            {!! Form::select('data[course_type]', $courses, null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Kursus pricer') !!}
            {!! Form::select('data[course_price]', $prices, null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Teacher') !!}
            {!! Form::select('parent_id', $teachers, null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group ">
            {!! Form::label('CPR nr') !!}
            {!! Form::number('data[code]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Telefon') !!}
            {!! Form::number('data[phone]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Adresse') !!}
            {!! Form::text('data[address]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Adresse 2') !!}
            {!! Form::text('data[address2]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Postnr') !!}
            {!! Form::number('data[post_index]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('By') !!}
            {!! Form::text('data[city]', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Kommentar') !!}
            {!! Form::textarea('data[description]', null, ['class' => 'form-control', 'rows' => '5']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Førstehjælps kursus') !!}
            {!! Form::select('data[s1]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Lægeerklæring') !!}
            {!! Form::select('data[s2]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Godkendt af kommune') !!}
            {!! Form::select('data[s3]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Teori prøve') !!}
            {!! Form::select('data[s4]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Praktisk prøve') !!}
            {!! Form::select('data[s5]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-actions">
        {!! Form::submit($button_label, ['class' => 'btn blue']) !!}
    </div>
</div>




