@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="col-md-9">
                <a href="{{route('student.index')}}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back To Students</a>
                <a href="{{route('student.edit', $student->id)}}" class="btn btn-info"><i class="fa fa-edit"></i> Edit Student</a>
                <br><br>
                <a href="{{route('student.add_booking', $student->id)}}" class="btn btn-info"><i class="fa fa-plus"></i> Add Booking</a>
            </div>
            <div class="col-md-3">
                <a href="{{route('student.calendar', $student->id)}}" class="btn btn-info"><i class="fa fa-calendar-plus-o"></i> Bookings</a>
                <a href="" class="btn btn-info"><i class="fa fa-list-ul"></i> Lection Plan</a><br><br>
                <a href="#student-{{$student->id}}" data-toggle="modal" class="btn btn-warning"><i class="fa fa-book"></i> Archive </a>
                {!! Form::open(['url' => route('student.destroy',$student->id), 'id' => 'delete-form-'.$student->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                {{Form::close()}}
                <div class="modal fade draggable-modal" id="student-{{$student->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            </div>
                            <div class="modal-body"> Are you sure you want to archive {{$student->name}}? </div>
                            <div class="modal-footer">
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                                <a onclick="document.getElementById('delete-form-{{ $student->id }}').submit();" class="btn green">Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6"><br><br>
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Teacher information</div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Phone </th>
                                    <th> Email	 </th>
                                    <th> Address	 </th>
                                    <th> Address2	 </th>
                                    <th> Zip	 </th>
                                    <th> City	 </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> {{$student->name}}</td>
                                    <td> {{$student->data->last_name}} </td>
                                    <td> {{$student->data->phone}} </td>
                                    <td> {{$student->email}} </td>
                                    <td> {{$student->data->address}} </td>
                                    <td> {{$student->data->address2}} </td>
                                    <td> {{$student->data->post_index}} </td>
                                    <td> {{$student->data->city}} </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
                <div class="col-md-6">
                    <h3>Additional information</h3>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th> Teams </th>
                            <td> @foreach($student->teams as $team) {{$team->name}},<br>  @endforeach</td>
                        </tr>
                        <tr>
                            <th> Lection plan</th>
                            <td>  </td>
                        </tr>
                        <tr>
                            <th> Course type</th>
                            <td> @foreach($course as $item) {{$item->name}} @endforeach</td>
                        </tr>
                        <tr>
                            <th> Course price</th>
                            <td> @foreach($price as $item) {{$item->name}} @endforeach</td>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
            <div class="col-md-6"><br><br>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Approvals</div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <div class="col-md-12">
                                <div class="mt-element-list">
                                    <div class="mt-list-container list-default">

                                        <ul>
                                            <li class="mt-list-item">
                                                @if($student->data->s1 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                                    @else
                                                    <div class="list-icon-container">
                                                        <i class="icon-close"></i>
                                                    </div>
                                                @endif
                                                <div class="list-item-content">
                                                    <h3 class="uppercase bold">
                                                        Lægeerklæring
                                                    </h3>
                                                </div>
                                            </li>
                                            <li class="mt-list-item">
                                                @if($student->data->s2 == '1')
                                                    <div class="list-icon-container done">
                                                        <i class="icon-check"></i>
                                                    </div>
                                                @else
                                                    <div class="list-icon-container ">
                                                        <i class="icon-close"></i>
                                                    </div>
                                                @endif
                                                <div class="list-item-content">
                                                    <h3 class="uppercase bold">
                                                        Førstehjælps kursus
                                                    </h3>
                                                </div>
                                            </li>
                                            <li class="mt-list-item">
                                                @if($student->data->s3 == '1')
                                                    <div class="list-icon-container done">
                                                        <i class="icon-check"></i>
                                                    </div>
                                                @else
                                                    <div class="list-icon-container">
                                                        <i class="icon-close"></i>
                                                    </div>
                                                @endif
                                                <div class="list-item-content">
                                                    <h3 class="uppercase bold">
                                                        Godkendt af kommune
                                                    </h3>
                                                </div>
                                            </li>
                                            <li class="mt-list-item">
                                                @if($student->data->s4 == '1')
                                                    <div class="list-icon-container done">
                                                        <i class="icon-check"></i>
                                                    </div>
                                                @else
                                                    <div class="list-icon-container">
                                                        <i class="icon-close"></i>
                                                    </div>
                                                @endif
                                                <div class="list-item-content">
                                                    <h3 class="uppercase bold">
                                                        Teori prøve
                                                    </h3>
                                                </div>
                                            </li>
                                            <li class="mt-list-item">
                                                @if($student->data->s5 == '1')
                                                    <div class="list-icon-container done">
                                                        <i class="icon-check"></i>
                                                    </div>
                                                @else
                                                    <div class="list-icon-container">
                                                        <i class="icon-close"></i>
                                                    </div>
                                                @endif
                                                <div class="list-item-content">
                                                    <h3 class="uppercase bold">
                                                        Praktisk prøve
                                                    </h3>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                    </div>
            </div>
        </div>
    </div>

@endsection