@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Add Booking to student</h1>
            <div class="portlet light bordered">
                {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                @include('admin.student._partials._booking', ['button_label' => 'Save'])
                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection