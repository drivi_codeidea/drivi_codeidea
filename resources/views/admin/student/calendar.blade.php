@extends('layouts.app')
@section('content')

    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Your Booking</h1>
            <div class="col-md-9"><br>
                <a href="{{route('student.index')}}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back To Students</a>
                <a href="{{route('student.show', $student->id)}}" class="btn btn-info"><i class="fa fa-user"></i> View Student</a>

                <a href="{{route('student.edit', $student->id)}}" class="btn btn-info"><i class="fa fa-edit"></i> Edit Student</a>
                <br><br>
                <a href="{{route('student.add_booking', $student->id)}}" class="btn btn-info"><i class="fa fa-plus"></i> Add Booking</a>
            </div>
            <div class="col-md-3"><br>
                <a href="" class="btn btn-info"><i class="fa fa-list-ul"></i> Lection Plan</a>
                <a href="#student-{{$student->id}}" data-toggle="modal" class="btn btn-warning"><i class="fa fa-book"></i> Archive </a>
                {!! Form::open(['url' => route('student.destroy',$student->id), 'id' => 'delete-form-'.$student->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                {{Form::close()}}
                <div class="modal fade draggable-modal" id="student-{{$student->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            </div>
                            <div class="modal-body"> Are you sure you want to archive {{$student->name}}? </div>
                            <div class="modal-footer">
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                                <a onclick="document.getElementById('delete-form-{{ $student->id }}').submit();" class="btn green">Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light calendar bordered">
                <div class="portlet-body">
                    <div id="drivi-calendar"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('includes.calendar')