 <br><br>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed "  style="padding-top: 20px">

            <li class="">
                <a href="{{route('admin.dashboard')}}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('student.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Student</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('teacher.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-user-plus"></i>
                    <span class="title">Teacher</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('team.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Team</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('booking.index')}}" class="nav-link nav-toggle">
                    <i class="fa fa-calendar"></i>
                    <span class="title">Booking</span>
                </a>
            </li>
            <h3 class="uppercase"></h3>
            </li>
            <li class="nav-item  ">
                <a href="{{route('plan')}}" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Lectionplan</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('course.index')}}" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Course Type</span>
                </a>
            </li>
            <h3 class="uppercase"></h3>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Accounting</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{route('invoice')}}" class="nav-link ">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">Invoices</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{route('bill')}}" class="nav-link ">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">Bills</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->