@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Accounting > Bills</h1>
        </div>
    </div>
@endsection