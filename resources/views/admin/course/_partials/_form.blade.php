<div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
<div class="alert alert-success display-hide">
    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
{{ csrf_field() }}
@include('includes.messages')
<div class="form-body">
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {{Form::label('Name')}}
        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-get-pocket"></i>
            </span>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('name') }}</span>
    </div>
    <div class="form-actions">
        {!! Form::submit($button_label, ['class' => 'btn blue']); !!}
    </div>
</div>