@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <h1 class="page-title"> Rediger Kursus typer</h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            {!! Form::model($course, ['url' => route('course.update', $course->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
                @include('admin.course._partials._form', ['button_label' => 'Edit'])
            {{Form::close()}}
        </div>
    </div>

@endsection