@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <h1 class="page-title"> Course type & price
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

    <div class="row">
        <div class="col-md-12">
            <div class="btn-group">
                <a href="{{route('course.create')}}"  class="btn green"><i class="fa fa-plus"></i> Create new coure
                </a>
            </div>
            <div class="btn-group">
                <a href="{{route('price.create')}}" class="btn green"><i class="fa fa-plus"></i> Create new price
                </a>
            </div>
        </div>

        <div class="col-md-6">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                       Course type</div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <tbody>
                            @foreach($courses as $course)
                            <tr>
                                <td class="highlight">
                                    <a href="{{route('course.edit', $course->id)}}"> {{$course->name}} </a>
                                </td>
                                <td>
                                    {!! Form::open(['url' => route('course.destroy',$course->id), 'id' => 'delete-form-'.$course->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                                    {{Form::close()}}
                                    <a href="#course-{{$course->id}}" data-toggle="modal" > <i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <div class="modal fade draggable-modal" id="course-{{$course->id}}"  role="basic" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                                        </div>
                                        <div class="modal-body"> Are you sure, You Want to delete this? </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                            <a  type="button"   onclick="
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $course->id }}').submit();
                                                    " class="btn green">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
        <div class="col-md-6">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        Course price</div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <tbody>
                            @foreach($prices as $price)
                            <tr>
                                <td class="highlight">
                                    <a href="{{route('price.edit', $price->id)}}"> {{$price->name}} </a>
                                </td>
                                <td>
                                    {!! Form::open(['url' => route('price.destroy',$price->id), 'id' => 'delete-form-'.$price->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                                    {{Form::close()}}
                                    <a href="#price-{{$price->id}}" data-toggle="modal" > <i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <div class="modal fade draggable-modal" id="price-{{$price->id}}" tabindex="-1" role="basic" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        </div>
                                        <div class="modal-body"> Are you sure, You Want to delete this? </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                            <a  type="button"   onclick="
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $price->id }}').submit();
                                                    " class="btn green">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
        </div>
    </div>
@endsection