@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Add Booking to teacher
            </h1>
            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        {{ Form::open([ 'route' => 'booking.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                        @include('admin.teacher._partials._booking', ['button_label' => 'Save'])
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection