@extends('layouts.app')
@section('content')

    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Your Booking</h1>
            <div class="col-md-10"><br>
                <a href="{{route('teacher.index')}}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back To Teachers</a>
                <a href="{{route('teacher.show', $teacher->id)}}" class="btn btn-info"><i class="fa fa-user"></i> View Teacher</a>
                <a href="{{route('teacher.edit', $teacher->id)}}" class="btn btn-info"><i class="fa fa-edit"></i> Edit Teacher</a>
                <br><br>
                <a href="{{route('teacher.add_booking', $teacher->id)}}" class="btn btn-info"><i class="fa fa-plus"></i> Add Booking</a>
            </div>

            <div class="portlet light calendar bordered">

                <div class="portlet-body">
                    <div id="drivi-calendar"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('includes.calendar')