@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Lærer
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a href="{{route('teacher.create')}}" class="btn green"> Create New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table class="table datatable table-striped table-hover table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th> NAVN </th>
                                    <th> EFTERNAVN </th>
                                    <th> TELEFON </th>
                                    <th> EMAIL </th>
                                    <th> Booking </th>
                                    <th> Delete </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teachers as $teacher)
                                <tr>
                                    <td><a href="{{route('teacher.show', $teacher->id)}}">{{$teacher->name}}</a> </td>
                                    <td><a href="{{route('teacher.show', $teacher->id)}}"> {{$teacher->data->last_name}} </a></td>
                                    <td><a href="{{route('teacher.show', $teacher->id)}}">{{$teacher->data->phone}} </a></td>
                                    <td><a href="{{route('teacher.show', $teacher->id)}}">{{$teacher->email}} </a></td>
                                    <td>
                                        <a href="{{route('teacher.calendar', $teacher->id)}}"> <i class="fa fa-calendar-plus-o"></i> </a>
                                    </td>
                                    <td>
                                        {!! Form::open(['url' => route('teacher.destroy',$teacher->id), 'id' => 'delete-form-'.$teacher->id, 'method' => 'DELETE', 'style' => 'display: none']) !!}
                                        {{Form::close()}}
                                        <a href="#teacher-{{$teacher->id}}" data-toggle="modal" > <i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <div class="modal fade draggable-modal" id="teacher-{{$teacher->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            </div>
                                            <div class="modal-body"> Are you sure, You Want to delete {{$teacher->name}}? </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                <a onclick="document.getElementById('delete-form-{{ $teacher->id }}').submit();" class="btn green">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@include('includes.ordering')

