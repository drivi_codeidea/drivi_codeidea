@include('includes.messages')
<div class="form-body">
{{ csrf_field()}}
    <div class="form-body">
        <div class="form-group">
            {{Form::label('Lærer')}}
            {{ Form::text('teacher_id',  $teacher->name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
            {{ Form::hidden('teacher_id',  $teacher->id, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
                {{Form::label('Type')}}
                {{ Form::select('type',  $types, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
                {{Form::label('Elev')}}
                {{ Form::select('student_id',  $students, null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
            {{Form::label('Dato')}}
            {{ Form::text('date',  null, ['class' => 'input-group form-control form-control-inline date date-picker', 'data-date-format' => 'yyyy-mm-dd']) }}
            <span class="text-danger">{{ $errors->first('date') }}</span>
        </div>
        <div class="form-group {{ $errors->has('time_start') ? 'has-error' : '' }}">
            {{Form::label('Tid')}}
            {{ Form::text('time_start',  null, ['class' => 'form-control timepicker timepicker-24']) }}
            <span class="text-danger">{{ $errors->first('time_start') }}</span>
        </div>
        <div class="form-group {{ $errors->has('time_end') ? 'has-error' : '' }}">
            {{Form::label('Tid (end)')}}
            {{ Form::text('time_end',  null, ['class' => 'form-control timepicker timepicker-24']) }}
            <span class="text-danger">{{ $errors->first('time_end') }}</span>
        </div>
        <div class="form-group">
            {{Form::label('Kommentar')}}
            {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => '7']) }}
        </div>
        <div class="form-actions">
            {!! Form::submit($button_label, ['class' => 'btn blue']); !!}
        </div>
    </div>
</div>
@section('scripts')
    {!! Html::style(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'))  !!}
    {!! Html::style(asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css'))  !!}
    {!! Html::style(asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.cs'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'))  !!}
    {!! Html::script(asset('assets/pages/scripts/components-date-time-pickers.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'))  !!}
@endsection