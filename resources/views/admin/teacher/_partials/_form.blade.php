<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> You have some  errors. Please check below. </div>
    <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button> Your form validation is successful! </div>
    {{ csrf_field()}}
    <div class="form-body">

        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::hidden('role', 'teacher') !!}
            <label>Fornavn  <span class="required"> * </span> </label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group">
            <label>Efternavn  <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::text('data[last_name]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label>email  <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
               </div>
            <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            <label>Password  <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-key"></i>
                </span>
                <input type="password" class="form-control" name="password" value="{{old('password')}}">
            </div>
            <span class="text-danger">{{ $errors->first('password') }}</span>
        </div>

        <div class="form-group">
            <label>Telefon  <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::number('data[phone]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>Adresse  <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::text('data[address]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>Adresse 2  <span class="required"> * </span></label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                {!! Form::text('data[address2]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>Postnr <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::number('data[post_index]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>By <span class="required"> * </span></label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                {!! Form::text('data[city]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>Kommentar <span class="required"> * </span></label>
            {{ Form::textarea('data[description]', null, ['class' => 'form-control', 'rows' => '5']) }}
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn blue">{{$button_label}}</button>
    </div>
</div>

