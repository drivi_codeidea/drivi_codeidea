@extends('layouts.app')
@section('content')
    @include('admin.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
           <div class="col-md-10">
               <a href="{{route('teacher.index')}}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back To Teachers</a>
               <a href="{{route('teacher.edit', $teacher->id)}}" class="btn btn-info"><i class="fa fa-edit"></i> Edit Teacher</a>
               <br><br>
               <a href="{{route('teacher.add_booking', $teacher->id)}}" class="btn btn-info"><i class="fa fa-plus"></i> Add Booking</a>
           </div>
            <div class="col-md-2"><a href="{{route('teacher.calendar', $teacher->id)}}" class="btn btn-info"><i class="fa fa-calendar-plus-o"></i> Bookings</a></div>
            <div class="col-md-6"><br><br>
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Teacher information</div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Phone </th>
                                    <th> Email	 </th>
                                    <th> Address	 </th>
                                    <th> Address2	 </th>
                                    <th> Zip	 </th>
                                    <th> City	 </th>
                                    <th> Commentary	 </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> {{$teacher->name}}</td>
                                    <td> {{$teacher->data->last_name}} </td>
                                    <td> {{$teacher->data->phone}} </td>
                                    <td> {{$teacher->email}} </td>
                                    <td> {{$teacher->data->address}} </td>
                                    <td> {{$teacher->data->address2}} </td>
                                    <td> {{$teacher->data->city}} </td>
                                    <td> {{$teacher->data->post_index}} </td>
                                    <td> {{$teacher->data->description}} </td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-6"><br><br>
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Elev liste</div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                @foreach($teams as $team)
                                   <h3> {{$team->name}}</h3>
                                    @foreach($team->students as $student)
                                       <a href="{{route('student.show', $student->id)}}" target="_blank"> {{$student->name}}<br></a>
                                    @endforeach
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    @endsection