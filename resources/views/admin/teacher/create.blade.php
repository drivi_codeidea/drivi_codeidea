@extends('layouts.app')
@section('content')
@include('admin.layouts.sidebar')
@include('includes.messages')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Tilføj ny
            </h1>
            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">

                                <a href="{{route('teacher.index')}}" class="btn btn-default caption-subject bold uppercase"> <i class="fa fa-long-arrow-left"></i> Back to Lærer</a>
                            </div>
                        </div>
                        <div class="portlet-body ">
                            {{ Form::open([ 'route' => 'teacher.store',  'class' => 'form-group',  'id' => 'form_sample_2'])}}
                            @include('admin.teacher._partials._form', ['button_label' => 'Save'])
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection