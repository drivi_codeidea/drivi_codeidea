@extends('layouts.app')
@section('content')
@include('admin.layouts.sidebar')

<div class="page-content-wrapper">
    <div class="page-content">
        <h1 class="page-title">  Tilføj ny</h1>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="col-md-9">
                    <a href="{{route('teacher.index')}}" class="btn btn-default"> <i class="fa fa-long-arrow-left"></i> Back to Teachers</a>
                    <a href="{{route('teacher.show', $teacher->id)}}" class="btn btn-info"><i class="fa fa-user"></i> View Teacher</a>
                    <br><br>
                    <a href="{{route('teacher.add_booking', $teacher->id)}}" class="btn btn-info"> <i class="fa fa-plus"></i>Add Booking</a>
                </div>
                <div class="col-md-3">
                    <a href="{{route('teacher.calendar', $teacher->id)}}" class="caption-subject bold uppercase btn btn-info"> <i class="fa fa-calendar-o"></i> Bookings</a>
                </div>
            </div>
        </div>
        <div class="portlet-body ">
            {!! Form::model($teacher, ['url' => route('teacher.update', $teacher->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
            @include('admin.teacher._partials._form', ['button_label' => 'Edit'])
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection