<br><br>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed "  style="padding-top: 20px">

            <li class="">
                <a href="{{route('student.dashboard')}}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('student.profile')}}" class="nav-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Profile</span>
                </a>
            </li>
            <h3 class="uppercase"></h3>
            </li>
            <li class="nav-item  ">
                <a href="{{route('student.plan')}}" class="nav-link nav-toggle">
                    <i class="fa fa-list"></i>
                    <span class="title">Lectionplan</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('student.progress')}}" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="title">Progress</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{route('student.booking')}}" class="nav-link nav-toggle">
                    <i class="fa fa-calendar"></i>
                    <span class="title">Booking</span>
                </a>
            </li>
            <h3 class="uppercase"></h3>
            <li class="nav-item  ">
                <a href="{{route('student.account')}}" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Account</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();" class="nav-link nav-toggle">
                    <i class="fa fa-sign-out"></i>
                    <span class="title">Log Out</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->