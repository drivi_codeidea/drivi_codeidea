@extends('layouts.app')
@section('content')
    @include('student.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Lection plan</h1>
            <div class="row">
                <h2 class="text-center">View full lection plan </h2><br>
                <div class="col-lg-4">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="mt-element-list">
                                <div class="mt-list-head list-default green-haze">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="list-head-title-container">
                                                <h3 class="list-title uppercase sbold">Check list</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-list-container list-default">
                                    <ul>
                                        <li class="mt-list-item">
                                            @if($user->data->s1 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                            @else
                                                <div class="list-icon-container">
                                                    <i class="icon-close"></i>
                                                </div>
                                            @endif
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Lægeerklæring
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            @if($user->data->s2 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                            @else
                                                <div class="list-icon-container ">
                                                    <i class="icon-close"></i>
                                                </div>
                                            @endif
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Førstehjælps kursus
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            @if($user->data->s3 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                            @else
                                                <div class="list-icon-container">
                                                    <i class="icon-close"></i>
                                                </div>
                                            @endif
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Godkendt af kommune
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            @if($user->data->s4 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                            @else
                                                <div class="list-icon-container">
                                                    <i class="icon-close"></i>
                                                </div>
                                            @endif
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Teori prøve
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            @if($user->data->s5 == '1')
                                                <div class="list-icon-container done">
                                                    <i class="icon-check"></i>
                                                </div>
                                            @else
                                                <div class="list-icon-container">
                                                    <i class="icon-close"></i>
                                                </div>
                                            @endif
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Praktisk prøve
                                                </h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="mt-element-list">
                                <div class="mt-list-head list-simple font-white bg-red">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title">Your info</h3>
                                    </div>
                                </div>
                                <div class="mt-list-container list-simple">
                                    <ul>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">
                                                <h3 class="">
                                                    <b>Name:</b> {{$user->name}}
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">
                                                <h3 class="">
                                                    <b>Kat:</b> {{$user->role}}
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">
                                                <h3 class="">
                                                   <b>Student ID:</b> 00{{$user->id}}
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">

                                                <h3 class="">
                                                    <b>Lærer:</b> {{$teacher->name}}
                                                </h3>

                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">
                                                <h3 class="">
                                                   <b> Last lesson:</b>
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-item-content">
                                                <h3 class="">
                                                   <b> Next lesson:</b>
                                                </h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="mt-element-list">
                                <div class="mt-list-head list-news font-white bg-blue">
                                    <div class="list-head-title-container">
                                        <h3 class="list-title">Break Rules</h3>
                                    </div>
                                </div>
                                <div class="mt-list-container list-news">
                                    <ul>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime bold uppercase font-green"> 8 Nov, 2015 </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Latest news on Metronic</a>
                                                </h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum gravida mauris, a tincidunt dolor porttitor eu. </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection