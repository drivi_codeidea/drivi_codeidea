@extends('layouts.app')
@section('content')
    @include('student.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Your Bookings</h1>
            <div class="portlet light calendar bordered">
                <div class="portlet-title ">
                    <div class="caption">
                        <i class="icon-calendar font-dark hide"></i>
                        <a href="{{route('student.booking')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="drivi-calendar"> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('includes.calendar')
