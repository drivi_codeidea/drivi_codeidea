@extends('layouts.app')
@section('content')
    @include('student.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">Your Bookings</h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">

                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="btn-group">
                                            <a class="btn green" href="{{route('student.booking.calendar')}}">
                                                <i class="fa fa-calendar-plus-o"></i> Calendar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-hover table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th> TYPE </th>
                                    <th> DATO </th>
                                    <th> TID </th>
                                    <th> KOMMENTAR </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bookings as $booking)
                                <tr>
                                    <td><a href="#booking-{{$booking->id}}" data-toggle="modal">{{$booking->type}}</a></td>
                                    <td><a href="#booking-{{$booking->id}}" data-toggle="modal">{{$booking->date}}</a></td>
                                    <td><a href="#booking-{{$booking->id}}" data-toggle="modal">{{$booking->time_start}} - {{$booking->time_end}}</a></td>
                                    <td><a href="#booking-{{$booking->id}}" data-toggle="modal">{{$booking->description}}</a></td>
                                </tr>
                                <div class="modal fade draggable-modal" id="booking-{{$booking->id}}"  tabindex="-1" role="basic" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            </div>
                                            <div class="modal-body">
                                                <p><b>Booking Type: </b>{{$booking->type}}</p>
                                                <p><b>Time and Date: </b>{{$booking->date}}: {{$booking->time_start}} - {{$booking->time_end}}</p>
                                                <p><b>Teacher: </b> {{$booking->teacher->name}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    </div>
@endsection
@include('includes.ordering')