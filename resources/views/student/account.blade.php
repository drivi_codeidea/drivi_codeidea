@extends('layouts.app')
@section('content')
    @include('student.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Account settings</h1>
            <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet-body form">
                        {!! Form::model($user, ['url' => route('student.accountUpdate', $user->id), 'id' => 'form_sample_2', 'method' => 'PUT']) !!}
                        @include('student._partials._form', ['button_label' => 'Edit'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection