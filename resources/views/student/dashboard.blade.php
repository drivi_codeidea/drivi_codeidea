@extends('layouts.app')
@section('content')
    @include('student.layouts.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Dashboard
            </h1>
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-edit font-dark"></i>
                            <span class="caption-subject font-dark  uppercase">NOTIFICATIONS</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @if(count($bookings) > 0)
                            @foreach($bookings as $booking)
                                <div class="note note-info">
                                    <p class="pull-right">{{$booking->date}}: {{$booking->time_start}} - {{$booking->time_end}}</p>
                                    <h4 class="block">Booking Type: {{$booking->type}}</h4>
                                    <p style="color: gray">You have new booking.</p>
                                </div>
                            @endforeach
                        @else

                        @endif
                    </div>
                </div>
                <!-- END PORTLET-->
                <!-- BEGIN PROGRESS BARS PORTLET-->
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection