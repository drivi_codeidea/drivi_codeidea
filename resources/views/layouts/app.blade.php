<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Drivi</title>
    <link rel="icon" href="{{asset('assets/logo.png')}}" type="image/gif">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    {!! Html::style( asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/datatables/datatables.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') ) !!}
    {!! Html::style( asset('assets/global/css/components-md.min.css') ) !!}
    {!! Html::style( asset('assets/global/css/plugins-md.min.css') ) !!}
    {!! Html::style( asset('assets/layouts/layout/css/layout.min.css') ) !!}
    {!! Html::style( asset('assets/layouts/layout/css/themes/darkblue.min.css') ) !!}
    {!! Html::style( asset('assets/layouts/layout/css/custom.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/select2/css/select2.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') ) !!}
    {!! Html::style( asset('assets/global/plugins/fullcalendar/fullcalendar.min.css') ) !!}
    {!! Html::style( asset('assets/pages/css/error.min.css') ) !!}
    <!-- END THEME LAYOUT STYLES -->
<!-- END HEAD -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div class="page-wrapper">
    @include('layouts.header')

        @section('content')

        @show
</div>
<!-- BEGIN CORE PLUGINS -->
    {!! Html::script(asset('assets/global/plugins/jquery.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap/js/bootstrap.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/js.cookie.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery.blockui.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/moment.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/morris/morris.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/morris/raphael-min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/counterup/jquery.waypoints.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/counterup/jquery.counterup.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/fullcalendar/fullcalendar.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/horizontal-timeline/horizontal-timeline.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/flot/jquery.flot.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/flot/jquery.flot.resize.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/flot/jquery.flot.categories.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery.sparkline.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js'))  !!}
    {!! Html::script(asset('assets/global/scripts/datatable.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/datatables/datatables.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'))  !!}
    {!! Html::script(asset('assets/pages/scripts/table-datatables-editable.min.js'))  !!}
    {!! Html::script(asset('assets/global/scripts/app.min.js'))  !!}
    {!! Html::script(asset('assets/pages/scripts/dashboard.min.js'))  !!}
    {!! Html::script(asset('assets/layouts/layout/scripts/layout.min.js'))  !!}
    {!! Html::script(asset('assets/layouts/layout/scripts/demo.min.js'))  !!}
    {!! Html::script(asset('assets/layouts/global/scripts/quick-sidebar.min.js'))  !!}
    {!! Html::script(asset('assets/layouts/global/scripts/quick-nav.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'))  !!}
    {!! Html::script(asset('assets/global/plugins/select2/js/select2.full.min.js'))  !!}
    {!! Html::script(asset('assets/pages/scripts/components-select2.min.js'))  !!}
    {!! Html::style( asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') ) !!}
    {!! Html::script(asset('assets/global/plugins/bootstrap-toastr/toastr.min.js'))  !!}
    {!! Html::script(asset('assets/pages/scripts/ui-toastr.min.js'))  !!}
    @include('includes.messages')
    @yield('scripts')
<!-- END CORE PLUGINS -->
</body>
</html>