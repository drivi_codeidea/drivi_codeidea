@extends('layouts.app')
@section('content')<br><br><br><br>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div class="row">
    <div class="col-md-12 page-404">
        <div class="number font-red"> 404 </div>
        <div class="details" style="color: white">
            <h3>Oops! You're lost.</h3>
            <p > We can not find the page you're looking for.
                <br/>
                <a href="/"> Return home </a> or try the search bar below. </p>
        </div>
    </div>
</div>
@endsection