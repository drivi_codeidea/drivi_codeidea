<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin
Route::group(['namespace' => 'Admin', 'middleware' => ['admin', 'auth'], 'prefix' => 'admin'], function () {

    //  Dashboard
    Route::get('dashboard', 'DashboardController@dashboard')->name('admin.dashboard');

    //    Student
    Route::resource('student', 'StudentController');
    Route::group(['prefix' => 'student'], function (){
        Route::get('archive/all', 'StudentController@archive')->name('student.archive');
        Route::get('booking/{student}', 'StudentController@addBooking')->name('student.add_booking');
        Route::get('calendar/{student}', 'StudentController@calendar')->name('student.calendar');

    });

    //   Teacher
    Route::resource('teacher', 'TeacherController');
    Route::group(['prefix' => 'teacher'], function (){
        Route::get('booking/{teacher}', 'TeacherController@addBooking')->name('teacher.add_booking');
        Route::get('calendar/{teacher}', 'TeacherController@calendar')->name('teacher.calendar');
    });

    //   Team
    Route::resource('team', 'TeamsController');

    //    Booking
    Route::resource('booking', 'BookingsController');
    Route::group(['prefix' => 'booking'], function () {
        Route::get('create/{type}', 'BookingsController@create')->name('booking.create');
        Route::post('storeStudent', 'BookingsController@storeStudent')->name('booking.storeStudent');
        Route::post('store', 'BookingsController@store')->name('booking.store');
        Route::match(['put', 'patch'], 'updateStudent/{id}', 'BookingsController@updateStudent')->name('booking.updateStudent');
        Route::get('calendar/all', 'BookingsController@calendar')->name('booking.calendar');
        Route::get('archive/all', 'BookingsController@archive')->name('booking.archive');
    });

    //  Course
    Route::resource('course', 'CourseController');

    //  Price
    Route::resource('price', 'PriceController');

    //    Plan
    Route::get('plan', function () {
        return view('admin.plan.plan');
    })->name('plan');


    Route::get('accounting/invoice', function () {
        return view('admin.invoice');
    })->name('invoice');

    Route::get('accounting/bill', function () {
        return view('admin.bill');
    })->name('bill');
});

//Student
Route::group(['namespace' => 'Student', 'middleware' => ['auth', 'student'], 'prefix' => 'student'], function () {

    // Dashboard
    Route::get('dashboard', 'StudentController@dashboard')->name('student.dashboard');

    // Proflie
    Route::get('profile', 'StudentController@profile')->name('student.profile');

    // Plan
    Route::get('plan', 'StudentController@plan')->name('student.plan');

    // Progress
    Route::get('progress', 'StudentController@progress')->name('student.progress');

    //  Bookings
    Route::get('booking', 'StudentController@booking')->name('student.booking');

    Route::get('booking/calendar', 'StudentController@calendar')->name('student.booking.calendar');

    //  Account
    Route::get('account', 'StudentController@account')->name('student.account');

    // Edit Student
    Route::match(['put', 'patch'], 'accountUpdate/{student}', 'StudentController@accountUpdate')->name('student.accountUpdate');

});

//Teacher
Route::group(['namespace' => 'Teacher', 'middleware' => ['auth', 'teacher'], 'prefix' => 'teacher'], function () {

    // Dashboard
    Route::get('dashboard', 'TeacherController@dashboard')->name('teacher.dashboard');

    // Proflie
    Route::get('profile', 'TeacherController@profile')->name('teacher.profile');

    //    Students
    Route::get('student', 'TeacherController@student')->name('teacher.student');

    //  Bookings
    Route::get('booking', 'TeacherController@booking')->name('teacher.booking');

    // Account
    Route::get('account', 'TeacherController@account')->name('teacher.account');

    //  Edit Teacher
    Route::match(['put', 'patch'], 'accountUpdate/{teacher}', 'TeacherController@accountUpdate')->name('teacher.accountUpdate');

    // Calendar
   Route::get('booking/calendar', 'TeacherController@calendar')->name('teacher.booking.calendar');

    // Plan
    Route::get('plan', 'TeacherController@plan')->name('teacher.plan');


});

Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm')->name('index');

Route::get('/home', 'HomeController@index')->name('home');
