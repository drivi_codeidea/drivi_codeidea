<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $params = [
            'name'     => 'admin',
            'role'     => 'admin',
            'email'    => 'admin@admin.com',
            'password' => 'secret',
        ];

        $user = User::create($params);
        $userData = new \App\Models\UserData();
        $userData->user_id = $user->id;
        $userData->save();
    }
}
