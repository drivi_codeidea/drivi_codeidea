<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\BookingType;

class BookingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookingType::truncate();

        DB::table('booking_types')->insert([
            ['name'=>'Single practical'],
            ['name'=>'Theretical team booking'],
            ['name'=>'Practical team booking'],
            ['name'=>'Manøvrebane'],
            ['name'=>'Glatbane'],
            ['name'=>'Theoretical test'],
            ['name'=>'Practical test'],
        ]);
    }
}











