<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('student_id')->nullable()->index();
            $table->integer('teacher_id')->nullable()->index();
            $table->integer('team_id')->nullable()->index();
            $table->date('date')->index();
            $table->time('time_start')->index();
            $table->time('time_end')->index();
            $table->text('description');
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->integer('zip')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
