<?php

namespace App\Repositories;

use App\Contracts\UserDataContract;
use App\Models\UserData;

class UsersDataRepository implements UserDataContract
{
    private $userData;

    /**
     * UsersDataRepository constructor.
     * @param UserData $userData
     */
    public function __construct(UserData $userData)
    {
        $this->userData = $userData;

    }

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params)
    {
        return $this->userData->where('user_id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByAuthUserId($id)
    {
        return $this->userData->where('user_id', $id)->get();
    }
}