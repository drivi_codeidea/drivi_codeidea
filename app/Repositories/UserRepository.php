<?php

namespace App\Repositories;

use App\Contracts\UsersContract;
use App\Models\User;

class UserRepository implements UsersContract
{
    private $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return $this->user->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->user->find($id);
    }

    /**
     * @return mixed
     */
    public function getAllTeachers()
    {
        return $this->user->where('role', 'teacher')->with('data')->get();
    }

    /**
     * @return mixed
     */
    public function getAllStudents()
    {
        return $this->user->where('role', 'student')->get();
    }

    /**
     * @return mixed
     */
    public function getArchiveStudents()
    {
        return $this->user->where('role', 'student')->onlyTrashed()->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByParentId($id)
    {
        return $this->user->where('parent_id', $id)->with('data')->get();
    }
}