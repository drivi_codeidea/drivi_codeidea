<?php

namespace App\Repositories;

use App\Contracts\CourseContract;
use App\Models\Course;

class CoursesRepository implements CourseContract
{
    private $course;
    /**
     * CoursesRepository constructor.
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->course->create($params);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id)
    {
        return $this->course->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->course->where('id', $id)->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCourses()
    {
        return $this->course->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->course->where('id', $id)->get();
    }
}
