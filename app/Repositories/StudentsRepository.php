<?php

namespace App\Repositories;

use App\Contracts\StudentsContract;
use App\Models\Student;
use App\Models\UserData;
use App\Models\User;

class StudentsRepository implements StudentsContract
{
    private $user;

    /**
     * StudentsRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * create student
     *
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->user->create($params);
    }

    /**
     * @param $newStudent
     * @param $params
     */
    public function userIdAttache($newStudent, $params)
    {
        $userData = new UserData($params);
        $newStudent->data()->save($userData);
    }

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params)
    {

        unset($params['data']);
        return $this->user->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->user->where('id', $id)->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        return $this->user->hasOne('App\Models\UserData', 'user_id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->user->find($id);
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->user->find($id);
    }
}