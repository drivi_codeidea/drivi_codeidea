<?php

namespace App\Repositories;

use App\Contracts\TeachersContract;
use App\Models\User;
use App\Models\UserData;

class TeachersRepository implements TeachersContract
{
    private $user;

    /**
     * TeachersRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * create teacher
     *
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->user->create($params);
    }


    /**
     * @param $newTeacher
     * @param $params
     */
    public function userIdAttache($newTeacher, $params)
    {
        $userData = new UserData($params);
        $newTeacher->data()->save($userData);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($id, $params)
    {
        unset($params['data']);
        return $this->user->where('id', $id)->update($params);
    }

    /**
     * retrieve a teacher
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->user->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->user->where('id', $id)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->user->find($id);
    }
}