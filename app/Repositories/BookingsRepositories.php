<?php

namespace App\Repositories;

use App\Contracts\BookingsContract;
use App\Models\Booking;
use App\Models\BookingType;

class BookingsRepositories implements BookingsContract
{
    private $booking;
    /**
     * BookingsRepositories constructor.
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->booking->create($params);
    }

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params)
    {
        return $this->booking->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->booking->where('id', $id)->delete();
    }

    /**
     * @param $newBooking
     * @param $studentIds
     * @return bool
     */
    public function bookingStudentAttache($newBooking, $studentIds)
    {
        foreach ($studentIds as $id ) {
            $newBooking->students()->attach($id);
        }

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllBookings()
    {
        return $this->booking->orderBy('date', 'desc')->with('teacher')->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function bookingTypes()
    {
        return BookingType::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function bookingSelectedStudents($id)
    {
        return $this->booking->where('id', $id)->with('students')->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->booking->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByStudentId($id)
    {
        return $this->booking->where('student_id', $id)->with('students')->with('teacher')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByTeacherId($id)
    {
        return $this->booking->where('teacher_id', $id)->with('students')->with('student')->get();
    }

    /**
     * @return mixed
     */
    public function getArchiveBookings()
    {
        return $this->booking->onlyTrashed()->get();
    }

    /**
     * @param $params
     * @param null $id
     * @return mixed
     */
    public function getBookingTime($params, $id = null)
    {
        $bookings = $this->booking->where('date', $params->date)->where(function ($query) use ($params) {
            $query->where('time_end', $params->time_end)
                  ->where('time_start', $params->time_start)
                  ->orwhere(function ($q) use ($params) {
                        $q->where('time_start', '>', $params->time_start)
                           ->where('time_start', '<', $params->time_end);
                            })
                  ->orwhere(function ($queries) use ($params) {
                        $queries->where('time_end', '>', $params->time_start)
                                ->where('time_end', '<', $params->time_end);
                  });
        });
        if ($id) {
            $bookings = $bookings->where('id', $id);
        }

        return $bookings->get();
    }
}