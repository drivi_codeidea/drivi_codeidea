<?php

namespace App\Repositories;

use App\Contracts\PriceContract;
use App\Models\Price;

class PriceRepository implements PriceContract
{
    private $price;
    /**
     * PriceRepository constructor.
     * @param Price $price
     */
    public function __construct(Price $price)
    {
        $this->price = $price;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->price->create($params);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id)
    {
        return $this->price->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->price->where('id', $id)->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPrices()
    {
        return $this->price->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->price->where('id', $id)->get();
    }

}