<?php

namespace App\Repositories;

use App\Contracts\TeamsContract;
use App\Models\Team;

class TeamsRepository implements TeamsContract
{
    private $team;
    /**
     * TeamsRepository constructor.
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function store($params)
    {
        return $this->team->create($params);
    }

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params)
    {
        return $this->team->where('id', $id)->update($params);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->team->where('id', $id)->delete();
    }

    /**
     * @param $newTeam
     * @param $studentIds
     * @return bool|mixed
     */
    public function teamStudentAttache($newTeam, $studentIds)
    {
        foreach ($studentIds as $id ) {
             $newTeam->students()->attach($id);
        }
        return true;
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->team->where('id', $id)->with('students')->get();
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->team->find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllTeams()
    {
        return $this->team->with('students','teacher')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTeamByTeacherId($id)
    {
        return $this->team->where('teacher_id', $id)->with('students')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTeamByStudentId($id)
    {
        return $this->team->with('students')->where('student_id', $id)->get();
    }

}