<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Booking extends Model
{
    protected $table = 'bookings';

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'student_id',
        'teacher_id',
        'team_id',
        'date',
        'time_start',
        'time_end',
        'description',
        'city',
        'address',
        'zip',
        'type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function students()
    {
        return $this->belongsToMany(User::class, 'booking_student', 'booking_id', 'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher()
    {
        return $this->belongsTo(User::class,'teacher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(User::class,'student_id');
    }

    /**
     * @param array $dates
     * @return Booking
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

}
