<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'user_datas';

    protected $fillable = [
        'id',
        'last_name',
        'user_id',
        'code',
        'phone',
        'address',
        'address2',
        'city',
        'description',
        'course_price',
        'course_type',
        'post_index',
        's1',
        's2',
        's3',
        's4',
        's5',
    ];

}
