<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Teacher;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the teacher.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Teacher  $teacher
     * @return mixed
     */
    public function view(User $user, Teacher $teacher)
    {
        if ($user->role == 'admin')
        {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create teachers.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->role == 'admin')
        {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the teacher.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Teacher  $teacher
     * @return mixed
     */
    public function update(User $user, Teacher $teacher)
    {
        if ($user->role == 'admin')
        {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the teacher.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Teacher  $teacher
     * @return mixed
     */
    public function delete(User $user, Teacher $teacher)
    {
        if ($user->role == 'admin')
        {
            return true;
        }
        return false;
    }
}
