<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('student');

        $rules = [
            'email'    => ['required',
                           Rule::unique('users')->ignore($id)
                         ],
            'name'     => 'required',
        ];
        if ($this->filled('password') && $this->input('password')) {
            $rules['password'] = 'min:6|max:30';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function inputs()
    {
        $request = $this->except(['_token', '_method', 'new_password', 'old_password']);
        if (!$request['password']) {
            unset($request['password']);
        } else {
            $request['password'] = bcrypt($request['password']);
        }
        return $request;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => 'Teacher name is required',
            'email.required'  => 'Teacher email is required',
        ];
    }
}
