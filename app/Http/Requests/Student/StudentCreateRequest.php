<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class StudentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'email'    => 'required|unique:users',
            'password' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'course_price.required' => 'Course Price is required',
            'course_type.required'  => 'Course Type is required',
            'teacher_id.required'   => 'Teacher is required',
            'code.required'         => 'Code is required',
            'name.required'         => 'Name is required',
            'last_name.required'    => 'Last Name is required',
            'email.required'        => 'Email is required',
            'phone.required'        => 'Phone is required',
            'address.required'      => 'Address is required',
            'zip.required'          => 'Zip is required',
            'city.required'         => 'City is required',
            'description.required'  => 'Description is required',
        ];
    }
}
