<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;


class TeacherCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'email'     => 'required|unique:users',
            'password'  => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => 'Teacher name is required',
            'name.min'  => 'Teacher name is short',
            'email.required'  => 'Teacher email is required',
            'password.required' => 'Password Required'
        ];
    }
}
