<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class BookingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'date.required'  => 'Booking date is required',
            'description.required'  => 'Description is required',
            'time_start.required'  => 'Time start is required',
            'time_end.required'  => 'Time end is required',
        ];
    }

}
