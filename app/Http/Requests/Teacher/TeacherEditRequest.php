<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeacherEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('teacher');
        $rules = [
            'email'    => ['required',
                           Rule::unique('users')->ignore($id)
                        ],
            'name'     => 'required',
        ];
        if ($this->filled('password') && $this->input('password')) {
            $rules['password'] = 'min:6|max:30';
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => 'Teacher name is required',
            'email.required'  => 'Teacher email is required',
        ];
    }

    /**
     * @return array
     */
    public function inputs()
    {
        $request = $this->except(['_token', '_method', 'new_password', 'old_password']);
        if (!$request['password']) {
            unset($request['password']);
        } else {
            $request['password'] = bcrypt($request['password']);
        }
        return $request;
    }
}
