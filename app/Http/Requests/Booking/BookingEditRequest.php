<?php

namespace App\Http\Requests\Booking;

use Illuminate\Foundation\Http\FormRequest;

class BookingEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->get('type');

        $rules = [
            'date'        => 'required|after:yesterday',
            'time_start'  => 'required|after:yesterday',
            'time_end'    => 'required|after:time_start',
            'description' => 'required',
        ];

        switch ($request) {

            case 'Single practical':
                $rules['student_id'] = 'required';
                $rules['teacher_id'] = 'required';
                break;

            case 'Practical team booking':
                $rules['team_id'] = 'required';
                break;

            case 'Theretical team booking':
                $rules['team_id'] = 'required';
                break;

            case 'Manøvrebane':
                $rules['student_id'] = 'required';
                $rules['teacher_id'] = 'required';
                break;

            case 'Glatbane':
                $rules['student_id'] = 'required';
                $rules['teacher_id'] = 'required';
                break;

            case 'Theoretical test':
                $rules['student_id'] = 'required';
                $rules['city'] = 'required';
                $rules['address'] = 'required';
                $rules['zip'] = 'required';
                break;

            case 'Practical test':
                $rules['teacher_id'] = 'required';
                $rules['student_id'] = 'required';
                $rules['city'] = 'required';
                $rules['address'] = 'required';
                $rules['zip'] = 'required';
                break;
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'team_id.required'     => 'Team is Required',
            'city.required'        => 'City is Required',
            'address.required'     => 'Address is Required',
            'zip.required'         => 'Zip code is Required',
            'student_id.required'  => 'Student is Required',
            'teacher_id.required'  => 'Teacher is Required',
            'date.required'        => 'Booking date is required',
            'description.required' => 'Description is required',
            'time_start.required'  => 'Time start is required',
            'time_end.required'    => 'Time end is required',
        ];
    }

    /**
     * @return array
     */
    public function inputs()
    {
        $request = $this->except(['_token', '_method', 'student_id']);

        return $request;
    }
}
