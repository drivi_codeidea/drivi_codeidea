<?php

namespace App\Http\Requests\Team;

use Illuminate\Foundation\Http\FormRequest;

class TeamCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'teacher_id' => 'required',
            'student_id' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function inputs()
    {
        $request = $this->except('_token', '_method', 'student_id');

        return $request;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => 'Team name is required',
            'description.required'  => 'Team decription is required',
            'teacher_id.required'  => 'No teacher are added.',
            'student_id.required'  => 'No students are added.',

        ];
    }
}
