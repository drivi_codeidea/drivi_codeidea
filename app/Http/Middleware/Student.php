<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->role == 'student'){
            return $next($request);
        } else if ($user->role == 'teacher') {
            return redirect()->route('teacher.profile');
        } else {
            return redirect()->route('admin.dashboard');
        }

    }
}
