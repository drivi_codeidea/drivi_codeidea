<?php

namespace App\Http\Controllers\Teacher;

use App\Contracts\TeachersContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Teacher\TeacherEditRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Contracts\UsersContract;
use App\Contracts\UserDataContract;
use App\Contracts\BookingsContract;

class TeacherController extends Controller
{
    protected $usersRepo;
    protected $userDataRepo;
    protected $bookingRepo;
    protected $teacherRepo;

    /**
     * StudentController constructor.
     * @param UsersContract $usersContract
     * @param UserDataContract $userDataContract
     * @param BookingsContract $bookingsContract
     * @param TeachersContract $teachersContract
     */
    public function __construct(TeachersContract $teachersContract,UsersContract $usersContract, UserDataContract $userDataContract, BookingsContract $bookingsContract)
    {
        $this->usersRepo = $usersContract;
        $this->userDataRepo = $userDataContract;
        $this->bookingRepo = $bookingsContract;
        $this->teacherRepo = $teachersContract;
    }
    /**
     * Teacher Dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByTeacherId($user->id);

        return view('teacher.dashboard', compact('bookings'));
    }
    /**
     * Teacher Profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        $user = Auth::user();
        $user->load('data', 'teamTeacher');

        return view('teacher.profile', compact('userData', 'user'));
    }

    /**
     * Teacher Students
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function student()
    {
        $user = Auth::user();
        $students = $this->usersRepo->getByParentId($user->id);

        return view('teacher.student', compact('students'));
    }

    /**
     * Teacher Booking
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function booking()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByTeacherId($user->id);

        return view('teacher.booking', compact('bookings', 'user'));
    }

    /**
     * Teacher Lection Plan
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function plan()
    {
        return view('teacher.plan');
    }

    /**
     * Teacher Account Settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function account()
    {
        $user = Auth::user();

        return view('teacher.account', compact('user'));
    }

    /**
     * @param TeacherEditRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accountUpdate(TeacherEditRequest $request, $id)
    {
        $user = Auth::user();

        if (Hash::check($request['old_password'], $user->password)){

            if ($request->new_password == $request->password){

                $request = $request->inputs();

                try {
                    DB::beginTransaction();
                    $this->teacherRepo->update($id,$request);
                    $this->userDataRepo->update($id, $request['data']);
                    DB::commit();
                } catch (\Exception $exception) {
                    Session::flash('error', $exception->getMessage());
                    DB::rollBack();

                    return redirect()->back();
                }
                return redirect(route('teacher.profile'))->with('success', 'Your Data Edited successfully!');
            } else {
                Session::flash('error', 'Passwords do not match');

                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Old password does not match');

            return redirect()->back();
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByTeacherId($user->id);

        return view('teacher.calendar', compact('bookings'));
    }
}
