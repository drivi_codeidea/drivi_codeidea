<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'admin')
        {
            return redirect()->route('admin.dashboard');
        }
        if (Auth::user()->role == 'student')
        {
            return redirect()->route('student.dashboard');
        }
        if (Auth::user()->role == 'teacher')
        {
            return redirect()->route('teacher.dashboard');
        }

    }
}
