<?php

namespace App\Http\Controllers\Admin;

use App\Models\Price;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Price\PriceCreateRequest;
use App\Contracts\PriceContract;
use Session;

class PriceController extends Controller
{
    protected $pricesRepo;

    /**
     * PriceController constructor.
     * @param PriceContract $priceContract
     */
    public function __construct(PriceContract $priceContract)
    {
        $this->pricesRepo = $priceContract;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.price.create');
    }

    /**
     * @param PriceCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PriceCreateRequest $request)
    {
        $request = $request->except(['_token']);
        try {
            DB::beginTransaction();
            $this->pricesRepo->store($request);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('course.index'))->with('success', 'Course Price Created successfully!');
    }


    /**
     * Display the specified resource.
     *
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        return view('admin.price.edit', compact('price'));
    }

    /**
     * @param PriceCreateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PriceCreateRequest $request, $id)
    {
        $request = $request->except(['_token', '_method']);
        try {
            DB::beginTransaction();
            $this->pricesRepo->update($request, $id);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('course.index'))->with('success', 'Course Price Edited successfully!');
    }

    /**
     * @param PriceContract $priceRepo
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(PriceContract $priceRepo, $id)
    {
        $priceRepo->destroy($id);
        return redirect()->back();
    }
}
