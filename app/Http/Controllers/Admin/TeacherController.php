<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\TeachersContract;
use App\Contracts\UserDataContract;
use App\Contracts\BookingsContract;
use App\Contracts\TeamsContract;
use App\Contracts\UsersContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Teacher\TeacherCreateRequest;
use App\Http\Requests\Teacher\TeacherEditRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Session;


class TeacherController extends Controller
{
    protected $usersRepo;
    protected $teachersRepo;
    protected $usersDataRepo;
    protected $teamsRepo;
    protected $bookingsRepo;

    /**
     * TeacherController constructor.
     * @param UsersContract $usersContract
     * @param TeachersContract $teachersContract
     * @param UserDataContract $userDataContract
     * @param TeamsContract $teamsContract
     * @param BookingsContract $bookingsContract
     */
    public function __construct(UsersContract $usersContract, TeachersContract $teachersContract, UserDataContract $userDataContract, TeamsContract $teamsContract, BookingsContract $bookingsContract)
    {
        $this->usersRepo = $usersContract;
        $this->teachersRepo = $teachersContract;
        $this->usersDataRepo = $userDataContract;
        $this->teamsRepo = $teamsContract;
        $this->bookingsRepo = $bookingsContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = $this->usersRepo->getAllTeachers();

        return view('admin.teacher.teacher', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TeacherCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherCreateRequest $request)
    {
        $request = $request->except(['_token']);
        try {
            DB::beginTransaction();
            $newTeacher = $this->teachersRepo->store($request);
            $this->teachersRepo->userIdAttache($newTeacher, $request['data']);
            DB::commit();

            return redirect(route('teacher.index'))->with('success', 'Teacher Created successfully!');
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }
    }

    /**
     * @param User $teacher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $teacher)
    {
        $teams = $this->teamsRepo->getTeamByTeacherId($teacher->id);

        return view('admin.teacher.show', compact('teacher', 'teams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $teacher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $teacher)
    {
        return view('admin.teacher.edit', compact('teacher'));
    }


    /**
     * @param TeacherEditRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(TeacherEditRequest $request, $id)
    {
        $request = $request->inputs();

        try {
            DB::beginTransaction();
            $this->teachersRepo->update($id,$request);
            $this->usersDataRepo->update($id, $request['data']);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('teacher.index'))->with('success', 'Teacher Edited successfully!');

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->teachersRepo->destroy($id);
        return redirect()->back();
    }

    /**
     *
     * Create Booking
     *
     * @param User $teacher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addBooking(User $teacher)
    {
        $types = $this->bookingsRepo->bookingTypes()->pluck('name', 'name')->prepend('-- Select Type --', '');
        $students = $this->usersRepo->getAllStudents()->pluck('name', 'id')->prepend('-- Select Student --', '');

        return view('admin.teacher.booking', compact('teacher', 'types', 'students'));
    }

    /**
     * Show Bookings In Calendar
     *
     * @param User $teacher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(User $teacher)
    {
        $bookings = $this->bookingsRepo->getByTeacherId($teacher->id);

        return view('admin.teacher.calendar', compact('teacher', 'bookings'));
    }

}
