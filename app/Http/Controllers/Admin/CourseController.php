<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\CourseContract;
use App\Contracts\PriceContract;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Http\Requests\Course\CourseCreateRequest;
use Illuminate\Support\Facades\DB;
use Session;

class CourseController extends Controller
{
    protected $coursesRepo;
    protected $pricesRepo;

    /**
     * CourseController constructor.
     * @param CourseContract $courseContract
     * @param PriceContract $priceContract
     */
    public function __construct(CourseContract $courseContract, PriceContract $priceContract)
    {
        $this->coursesRepo = $courseContract;
        $this->pricesRepo = $priceContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = $this->coursesRepo->getAllCourses();
        $prices = $this->pricesRepo->getAllPrices();

        return view('admin.course.course', compact('courses','prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.course.create');
    }

    /**
     * @param CourseCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CourseCreateRequest $request)
    {
        $request = $request->except(['_token']);
        try {
            DB::beginTransaction();
            $this->coursesRepo->store($request);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('course.index'))->with('success', 'Course Type Created successfully!');
    }

    /**
     * @param Course $course
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('admin.course.edit', compact('course'));
    }

    /**
     * @param CourseCreateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CourseCreateRequest $request, $id)
    {
        $request = $request->except(['_token', '_method']);
        try {
            DB::beginTransaction();
            $this->coursesRepo->update($request, $id);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('course.index'))->with('success', 'Course Type Edited successfully!');
    }

    /**
     * @param $id
     * @param CourseContract $courseRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, CourseContract $courseRepo)
    {
        $courseRepo->destroy($id);
        return redirect()->back();
    }
}
