<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\UsersContract;
use App\Contracts\BookingsContract;

class DashboardController extends Controller
{
    protected $usersRepo;
    protected $bookingRepo;

    /**
     * DashboardController constructor.
     * @param UsersContract $usersContract
     * @param BookingsContract $bookingsContract
     */
    public function __construct(UsersContract $usersContract, BookingsContract $bookingsContract)
    {
        $this->usersRepo = $usersContract;
        $this->bookingRepo = $bookingsContract;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $students = $this->usersRepo->getAllStudents();
        $teachers = $this->usersRepo->getAllTeachers();
        $bookings = $this->bookingRepo->getAllBookings();

        return view('admin.dashboard', compact('students', 'teachers', 'bookings'));
    }
}
