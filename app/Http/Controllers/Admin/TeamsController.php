<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Http\Requests\Team\TeamCreateRequest;
use App\Contracts\TeamsContract;
use App\Contracts\UsersContract;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{

    protected $teamsRepo;
    protected $usersRepo;

    /**
     * TeamsController constructor.
     *
     * @param TeamsContract $teamsContract
     * @param UsersContract $usersContract
     */
    public function __construct(TeamsContract $teamsContract, UsersContract $usersContract)
    {
        $this->teamsRepo = $teamsContract;
        $this->usersRepo = $usersContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = $this->teamsRepo->getAllTeams();

        return view('admin.team.team', compact('teams', 'teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');
        $students = $this->usersRepo->getAllStudents()->pluck('name', 'id');

        return view('admin.team.create', compact('teachers', 'students'));
    }

    /**
     * @param TeamCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TeamCreateRequest $request)
    {
        $studentIds = $request->get('student_id');
        $request = $request->inputs();

        try {
            DB::beginTransaction();
            $newTeam = $this->teamsRepo->store($request);
            $this->teamsRepo->teamStudentAttache($newTeam, $studentIds);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('team.index'))->with('success', 'Team Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Team $team
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Team $team)
    {
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');
        $students = $this->usersRepo->getAllStudents()->pluck('name', 'id');

        return view('admin.team.edit', compact('team', 'teachers', 'students'));
    }


    /**
     * @param TeamCreateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(TeamCreateRequest $request, $id)
    {
        $studentIds = $request->get('student_id');
        $request = $request->inputs();

        try {
            DB::beginTransaction();

            $this->teamsRepo->update($id, $request);
            $team = Team::find($id);
            $team->students()->sync($studentIds);

            DB::commit();
        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('team.index'))->with('success', 'Team Edited successfully!');
    }

    /**
     * @param $id
     * @param TeamsContract $teamRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, TeamsContract $teamRepo)
    {
        $teamRepo->destroy($id);
        return redirect()->back();
    }
}
