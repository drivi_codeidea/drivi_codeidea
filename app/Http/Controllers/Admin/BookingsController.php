<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Booking\BookingCreateRequest;
use App\Http\Requests\Booking\BookingEditRequest;
use App\Models\Booking;
use App\Models\BookingType;
use App\Contracts\BookingsContract;
use App\Contracts\UsersContract;
use App\Contracts\TeamsContract;
use Illuminate\Support\Facades\DB;
use Session;

class BookingsController extends Controller
{
    protected $bookingsRepo;
    protected $usersRepo;
    protected $teamsRepo;

    /**
     * BookingsController constructor.
     * @param BookingsContract $bookingsContract
     * @param UsersContract $usersContract
     * @param TeamsContract $teamsContract
     */
    public function __construct(BookingsContract $bookingsContract, UsersContract $usersContract, TeamsContract $teamsContract)
    {
        $this->bookingsRepo = $bookingsContract;
        $this->usersRepo = $usersContract;
        $this->teamsRepo = $teamsContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = $this->bookingsRepo->getAllBookings();
        $types = $this->bookingsRepo->bookingTypes();

        return view('admin.booking.booking', compact('bookings', 'types'));
    }

    /**
     * @param $bookingTypeId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($bookingTypeId)
    {
        $teachers = $this->usersRepo->getAllTeachers()->where('role', 'teacher')->pluck('name', 'id')->prepend('-- Select Teacher --', '');
        $students = $this->usersRepo->getAllStudents()->pluck('name', 'id')->prepend('-- Select Student --', '');
        $teams = $this->teamsRepo->getAllTeams()->pluck('name', 'id')->prepend('-- Select Team --', '');
        $student_get = $this->usersRepo->getAllStudents()->pluck('name', 'id');
        $bookingType = BookingType::find($bookingTypeId);

        return view('admin.booking.create', compact('teachers', 'students', 'bookingType', 'teams','student_get'));

    }

    /**
     * @param BookingCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BookingCreateRequest $request)
    {
        $bookTimeDate = $this->bookingsRepo->getBookingTime($request);

        if ($bookTimeDate->isEmpty()) {
            $request = $request->except('_token');
            try {
                DB::beginTransaction();
                $this->bookingsRepo->store($request);
                DB::commit();
            } catch (\Exception $exception) {
                Session::flash('error', $exception->getMessage());
                DB::rollBack();

                return redirect()->back();
            }

            return redirect(route('booking.index'))->with('success', 'Booking Created successfully!');
        } else {
            Session::flash('error', 'This time and date has already taken');

            return redirect()->back();
        }

    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @param BookingCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeStudent(BookingCreateRequest $request)
    {
        $bookTimeDate = $this->bookingsRepo->getBookingTime($request);

        if ($bookTimeDate->isEmpty()) {
            $studentIds = $request->get('student_id');
            $request = $request->except('_token', 'student_id');
            try {
                DB::beginTransaction();
                $newBooking = $this->bookingsRepo->store($request);
                $this->bookingsRepo->bookingStudentAttache($newBooking, $studentIds);
                DB::commit();
            } catch (\Exception $exception) {
                Session::flash('error', $exception->getMessage());
                DB::rollBack();

                return redirect()->back();
            }

            return redirect(route('booking.index'));
        } else {
            Session::flash('error', 'This time and date has already taken');

            return redirect()->back();
        }
    }

    /**
     * @param $booking
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Booking $booking)
    {
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');
        $students = $this->usersRepo->getAllStudents()->pluck('name', 'id')->prepend('-- Select Student --', '');
        $teams = $this->teamsRepo->getAllTeams()->pluck('name', 'id')->prepend('-- Select Team --', '');
        $student_get = $this->usersRepo->getAllStudents()->pluck('name', 'id');
        $bookSelectedStudents = $this->bookingsRepo->bookingSelectedStudents($booking->id);

        return view('admin.booking.edit', compact('booking', 'teachers', 'students', 'teams', 'student_get', 'bookSelectedStudents'));
    }

    /**
     * @param BookingEditRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(BookingEditRequest $request, $id)
    {
        $bookTimeDate = $this->bookingsRepo->getBookingTime($request, $id);

        if ($bookTimeDate->isEmpty()) {
            $request = $request->except([ '_token', '_method' ]);
            try {
                DB::beginTransaction();
                $this->bookingsRepo->update($id, $request);
                DB::commit();
            } catch (\Exception $exception) {
                Session::flash('error', $exception->getMessage());
                DB::rollBack();

                return redirect()->back();
            }

            return redirect(route('booking.index'))->with('success', 'Booking Edited successfully!');
        } else {
            Session::flash('error', 'This time and date has already taken');

            return redirect()->back();
        }
    }

    /**
     * @param BookingEditRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateStudent(BookingEditRequest $request, $id)
    {

        $bookTimeDate = $this->bookingsRepo->getBookingTime($request, $id);

        if ($bookTimeDate->isEmpty()) {
            $studentIds = $request->get('student_id');
            $request = $request->inputs();
            try {
                DB::beginTransaction();
                $this->bookingsRepo->update($id, $request);
                $booking = $this->bookingsRepo->getById($id);
                $booking->students()->sync($studentIds);
                DB::commit();
            } catch (\Exception $exception) {
                Session::flash('error', $exception->getMessage());
                DB::rollBack();

                return redirect()->back();
            }

            return redirect(route('booking.index'))->with('success', 'Booking Edited successfully!');
        } else {
            Session::flash('error', 'This time and date has already taken');

            return redirect()->back();
        }
    }

    /**
     * @param $id
     * @param BookingsContract $bookingRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, BookingsContract $bookingRepo)
    {
        $bookingRepo->destroy($id);
        return redirect()->back();
    }

    /**
     * @param Booking $booking
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(Booking $booking)
    {
        $bookings = $this->bookingsRepo->getAllBookings();
        $types = $this->bookingsRepo->bookingTypes();

        return view('admin.booking.calendar', compact('booking', 'types', 'bookings'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive()
    {
        $bookings = $this->bookingsRepo->getArchiveBookings();
        $types = $this->bookingsRepo->bookingTypes();

        return view('admin.booking.archive', compact('bookings', 'types'));
    }

}
