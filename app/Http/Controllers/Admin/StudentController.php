<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\StudentsContract;
use App\Contracts\UserDataContract;
use App\Contracts\UsersContract;
use App\Contracts\BookingsContract;
use App\Contracts\CourseContract;
use App\Contracts\PriceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StudentUpdateRequest;
use App\Models\User;
use App\Http\Requests\Student\StudentCreateRequest;
use Illuminate\Support\Facades\DB;
use Session;


class StudentController extends Controller
{
    protected $usersRepo;
    protected $usersDataRepo;
    protected $studentsRepo;
    protected $coursesRepo;
    protected $pricesRepo;
    protected $bookingRepo;

    /**
     * StudentController constructor.
     *
     * @param UsersContract $usersContract
     * @param StudentsContract $studentsContract
     * @param UserDataContract $userDataContract
     * @param CourseContract $courseContract
     * @param PriceContract $priceContract
     * @param BookingsContract $bookingsContract
     */
    public function __construct(BookingsContract $bookingsContract,UsersContract $usersContract, StudentsContract $studentsContract, UserDataContract $userDataContract, CourseContract $courseContract, PriceContract $priceContract)
    {
        $this->usersRepo = $usersContract;
        $this->studentsRepo = $studentsContract;
        $this->usersDataRepo = $userDataContract;
        $this->coursesRepo = $courseContract;
        $this->pricesRepo = $priceContract;
        $this->bookingRepo = $bookingsContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->usersRepo->getAllStudents();

        return view('admin.student.student', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = $this->coursesRepo->getAllCourses()->pluck('name', 'id')->prepend('-- Select Course --', '');
        $prices = $this->pricesRepo->getAllPrices()->pluck('name', 'id')->prepend('-- Select Price --', '');
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');

        return view('admin.student.create', compact('courses', 'prices', 'teachers'));
    }

    /**
     * @param StudentCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StudentCreateRequest $request)
    {
        $request = $request->except(['_token']);

        try {
            DB::beginTransaction();
            $newStudent = $this->studentsRepo->store($request);
            $this->studentsRepo->userIdAttache($newStudent, $request['data']);
            DB::commit();

            return redirect(route('student.index'))->with('success', 'Student Created successfully!');
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $student
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $student)
    {
        $price = $this->pricesRepo->getById($student->data->course_price);
        $course = $this->coursesRepo->getById($student->data->course_type);

        return view('admin.student.show', compact('student', 'price', 'course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $student
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $student)
    {
        $courses = $this->coursesRepo->getAllCourses()->pluck('name', 'id')->prepend('-- Select Course --', '');
        $prices = $this->pricesRepo->getAllPrices()->pluck('name', 'id')->prepend('-- Select Price --', '');
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');

        return view('admin.student.edit', compact('student','courses', 'prices', 'teachers'));
    }

    /**
     * @param StudentUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StudentUpdateRequest $request, $id)
    {
        $request = $request->inputs();

        try {
            DB::beginTransaction();
            $this->studentsRepo->update($id,$request);
            $this->usersDataRepo->update($id, $request['data']);
            DB::commit();
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
            DB::rollBack();

            return redirect()->back();
        }

        return redirect(route('student.index'))->with('success', 'Student Edited successfully!');
    }

    /**
     * @param $id
     * @param StudentsContract $studentRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, StudentsContract $studentRepo)
    {
        $studentRepo->destroy($id);

        return redirect()->route('student.index');
    }

    /**
     * @param $student
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addBooking(User $student)
    {
        $types = $this->bookingRepo->bookingTypes()->pluck('name', 'name')->prepend('-- Select Type --', '');
        $teachers = $this->usersRepo->getAllTeachers()->pluck('name', 'id')->prepend('-- Select Teacher --', '');

        return view('admin.student.booking', compact('student', 'types', 'teachers'));
    }

    /**
     * Show Bookings In Calendar
     *
     * @param User $student
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(User $student)
    {
        $bookings = $this->bookingRepo->getByStudentId($student->id);

        return view('admin.student.calendar', compact('student', 'bookings'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive()
    {
        $students = $this->usersRepo->getArchiveStudents();

        return view('admin.student.archive', compact('students'));
    }

}
