<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Contracts\UsersContract;
use App\Contracts\PriceContract;
use App\Contracts\StudentsContract;
use App\Contracts\TeamsContract;
use App\Contracts\CourseContract;
use App\Contracts\UserDataContract;
use App\Contracts\BookingsContract;
use App\Http\Requests\Student\StudentUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    protected $usersRepo;
    protected $userDataRepo;
    protected $bookingRepo;
    protected $pricesRepo;
    protected $coursesRepo;
    protected $studentRepo;
    protected $teamsRepo;

    /**
     * StudentController constructor.
     * @param UsersContract $usersContract
     * @param UserDataContract $userDataContract
     * @param BookingsContract $bookingsContract
     * @param PriceContract $priceContract
     * @param CourseContract $courseContract
     * @param StudentsContract $studentsContract
     * @param TeamsContract $teamsContract
     */
    public function __construct(TeamsContract $teamsContract,StudentsContract $studentsContract,UsersContract $usersContract, UserDataContract $userDataContract, BookingsContract $bookingsContract, PriceContract $priceContract, CourseContract $courseContract)
    {
        $this->usersRepo = $usersContract;
        $this->userDataRepo = $userDataContract;
        $this->bookingRepo = $bookingsContract;
        $this->pricesRepo = $priceContract;
        $this->coursesRepo = $courseContract;
        $this->studentRepo = $studentsContract;
        $this->teamsRepo = $teamsContract;
    }

    /**
     * Student Dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByStudentId($user->id);

        return view('student.dashboard', compact('bookings'));
    }

    /**
     * Student Profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        $user = Auth::user();
        $user->load('data', 'parents', 'teams');

        return view('student.profile', compact( 'user'));
    }

    /**
     * Student Lection Plan
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function plan()
    {
        $user = Auth::user();
        $prices = $this->pricesRepo->getById($user->data->course_price);
        $courses = $this->coursesRepo->getById($user->data->course_type);

        return view('student.plan', compact('user', 'prices', 'courses'));

    }

    /**
     *Progress
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function progress()
    {
        $user = Auth::user();
        $teacher = $this->usersRepo->getById($user['parent_id']);

        return view('student.progress', compact('user', 'teacher'));
    }

    /**
     * Student Bookings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function booking()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByStudentId($user->id);

        return view('student.booking', compact('bookings'));
    }

    /**
     * Student Account Settings
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function account()
    {
        $user = Auth::user();

        return view('student.account', compact('user'));
    }


    /**
     * @param StudentUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accountUpdate(StudentUpdateRequest $request, $id)
    {
        $user = Auth::user();

        if (Hash::check($request['old_password'], $user->password)){

            if ($request->new_password == $request->password){

                $request = $request->inputs();
                try {
                    DB::beginTransaction();
                    $this->studentRepo->update($id,$request);
                    $this->userDataRepo->update($id, $request['data']);
                    DB::commit();
                } catch (\Exception $exception) {
                    Session::flash('error', $exception->getMessage());
                    DB::rollBack();
                    return redirect()->back();
                }

                return redirect(route('student.profile'))->with('success', 'Your Data Edited successfully!');
            } else {
                Session::flash('error', 'Passwords do not match');

                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Old password does not match');

            return redirect()->back();
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar()
    {
        $user = Auth::user();
        $bookings = $this->bookingRepo->getByStudentId($user->id);

        return view('student.calendar', compact('bookings'));
    }
}
