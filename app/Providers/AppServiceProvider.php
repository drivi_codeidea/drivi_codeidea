<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\TeachersContract',
            'App\Repositories\TeachersRepository'
        );

        $this->app->bind(
            'App\Contracts\CourseContract',
            'App\Repositories\CoursesRepository'
        );

        $this->app->bind(
            'App\Contracts\PriceContract',
            'App\Repositories\PriceRepository'
        );

        $this->app->bind(
            'App\Contracts\StudentsContract',
            'App\Repositories\StudentsRepository'
        );

        $this->app->bind(
            'App\Contracts\TeamsContract',
            'App\Repositories\TeamsRepository'
        );

        $this->app->bind(
            'App\Contracts\BookingsContract',
            'App\Repositories\BookingsRepositories'
        );

        $this->app->bind(
            'App\Contracts\UserDataContract',
            'App\Repositories\UsersDataRepository'
        );

        $this->app->bind(
            'App\Contracts\UsersContract',
            'App\Repositories\UserRepository'
        );

    }
}
