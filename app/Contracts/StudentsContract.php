<?php

namespace App\Contracts;

interface StudentsContract
{
    /**
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     *
     * @param $id
     * @return mixed
     */
    public function show($id);
}