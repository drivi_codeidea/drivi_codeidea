<?php

namespace App\Contracts;

interface TeamsContract
{
    /**
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $newTeam
     * @param $studentIds
     * @return mixed
     */
    public function teamStudentAttache($newTeam, $studentIds);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllTeams();

    /**
     * @param $id
     * @return mixed
     */
    public function getTeamByTeacherId($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getTeamByStudentId($id);

}