<?php

namespace App\Contracts;

interface BookingsContract
{
    /**
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $newBooking
     * @param $studentIds
     * @return mixed
     */
    public function bookingStudentAttache($newBooking, $studentIds);

    /**
     * @return mixed
     */
    public function getAllBookings();

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function bookingTypes();

    /**
     * @param $id
     * @return mixed
     */
    public function bookingSelectedStudents($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getByStudentId($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getByTeacherId($id);

    /**
     * @return mixed
     */
    public function getArchiveBookings();

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function getBookingTime($params, $id = null);
}