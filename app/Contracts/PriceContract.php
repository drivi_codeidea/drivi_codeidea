<?php

namespace App\Contracts;

interface PriceContract
{
    /**
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPrices();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}