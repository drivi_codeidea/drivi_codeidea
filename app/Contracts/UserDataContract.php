<?php

namespace App\Contracts;

interface UserDataContract
{
    /**
     * @param $id
     * @param $params
     * @return mixed
     */
    public function update($id, $params);

    /**
     * @param $id
     * @return mixed
     */
    public function getByAuthUserId($id);

}