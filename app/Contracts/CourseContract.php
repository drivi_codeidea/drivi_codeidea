<?php

namespace App\Contracts;

interface CourseContract
{
    /**
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $params
     * @return mixed
     */
    public function update($params, $id);

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCourses();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}