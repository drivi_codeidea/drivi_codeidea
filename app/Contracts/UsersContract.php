<?php

namespace App\Contracts;

interface UsersContract
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @return mixed
     */
    public function getAllTeachers();

    /**
     * @return mixed
     */
    public function getAllStudents();

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index();

    /**
     * @param $id
     * @return mixed
     */
    public function getByParentId($id);

    /**
     * @return mixed
     */
    public function getArchiveStudents();
}