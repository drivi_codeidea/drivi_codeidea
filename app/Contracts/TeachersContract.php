<?php

namespace App\Contracts;

interface TeachersContract
{
    /**
     * create new teacher
     *
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);
    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($id, $params);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);
}